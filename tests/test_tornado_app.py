"""
Tornado web testing
python3 -m unittest
"""
import sys
import json
import unittest
from tornado.testing import AsyncHTTPTestCase, ExpectLog

sys.path.append("./backend")
# pylint: disable=wrong-import-position
from core.application import make_web_app


class TestLFApp(AsyncHTTPTestCase):
    """
    Endpoint testing
    """

    def get_app(self):
        return make_web_app()

    def test_swagger(self):
        """test correct resolution of static json file"""
        with ExpectLog("tornado.access", ".*GET"):  # raises if no actual GET reached
            response = self.fetch("/swagger/swagger.json")
            self.assertEqual(response.code, 200)

    def test_loan_application_post(self):
        """initial loan test"""
        data = {
            "taxId": "123-45-6789",
            "businessName": "ABCorp",
            "requestAmount": 20000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 200)

    def test_loan_application_response(self):
        """test loan-app correct response structure"""
        data = {
            "taxId": "123-45-6789",
            "businessName": "ABCorp",
            "requestAmount": 20000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 200)
            data = json.loads(response.body)
            self.assertIn("code", data)
            self.assertIn("type", data)
            self.assertIn("message", data)

    def test_loan_application_response_declined(self):
        """test loan-app correct response"""
        data = {
            "taxId": "123-45-6789",
            "businessName": "ABCorp",
            "requestAmount": 60000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 200)
            data = json.loads(response.body)
            self.assertIn("type", data)
            self.assertEqual(data["type"], "Declined")

    def test_loan_application_response_approved(self):
        """test loan-app correct response"""
        data = {
            "taxId": "123-45-6789",
            "businessName": "ABCorp",
            "requestAmount": 20000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 200)
            data = json.loads(response.body)
            self.assertIn("type", data)
            self.assertEqual(data["type"], "Approved")

    def test_loan_application_response_undecided(self):
        """test loan-app correct response"""
        data = {
            "taxId": "123-45-6789",
            "businessName": "ABCorp",
            "requestAmount": 50000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 200)
            data = json.loads(response.body)
            self.assertIn("type", data)
            self.assertEqual(data["type"], "Undecided")

    def test_loan_application_response_declined_message(self):
        """test loan-app correct response _message"""
        data = {
            "taxId": "123-45-6789",
            "businessName": "ABCorp",
            "requestAmount": 60000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 200)
            data = json.loads(response.body)
            self.assertIn("message", data)
            self.assertEqual(
                data["message"], "We're sorry! your loan has been declined."
            )

    def test_loan_application_response_approved_message(self):
        """test loan-app correct response _message"""
        data = {
            "taxId": "123-45-6789",
            "businessName": "ABCorp",
            "requestAmount": 20000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 200)
            data = json.loads(response.body)
            self.assertIn("message", data)
            self.assertEqual(
                data["message"], "Congratulations! your loan has been approved."
            )

    def test_loan_application_response_undecided_message(self):
        """test loan-app correct response _message"""
        data = {
            "taxId": "123-45-6789",
            "businessName": "ABCorp",
            "requestAmount": 50000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 200)
            data = json.loads(response.body)
            self.assertIn("message", data)
            self.assertEqual(data["message"], "Sorry! your loan can't be decided.")

    def test_loan_application_response_invalid_body(self):
        """test loan-app check invalid body json"""
        data = "some {random} string"
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch("/api/v1/loan-application", method="POST", body=data)
            self.assertEqual(response.code, 400)
            data = json.loads(response.body)
            self.assertIn("message", data)
            self.assertEqual(data["message"], "Malformed JSON")

    def test_loan_application_response_invalid_dict(self):
        """test loan-app check invalid json"""
        data = {
            "taxex": "1234-56789",
            "businessName": "ABCorp",
            "requestAmount": 50000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 400)
            data = json.loads(response.body)
            self.assertIn("message", data)
            self.assertEqual(
                data["message"], "Please input  taxId, businessName and requestAmount"
            )

    def test_loan_application_response_invalid_taxid(self):
        """test loan-app check invalid taxid format"""
        data = {
            "taxId": "1234-56789",
            "businessName": "ABCorp",
            "requestAmount": 50000,
        }
        with ExpectLog("tornado.access", ".*POST"):  # raises if no actual POST reached
            response = self.fetch(
                "/api/v1/loan-application", method="POST", body=json.dumps(data)
            )
            self.assertEqual(response.code, 400)
            data = json.loads(response.body)
            self.assertIn("message", data)
            self.assertEqual(
                data["message"], "Please enter a Valid SSN, EIN or ITIN (9 digits)."
            )

    def test_schema_loan(self):
        """test correct resolution of static json file"""
        with ExpectLog("tornado.access", ".*GET"):  # raises if no actual GET reached
            response = self.fetch("/api/v1/schema/loan-application.json")
            self.assertEqual(response.code, 200)


if __name__ == "__main__":
    unittest.main()
