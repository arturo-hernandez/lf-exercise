# I. Exercise

## 1. Estimated Time

| **Tasks**                             | **Estimated Time** |
| ------------------------------------- | ------------------ |
| Architecture & DB (Research & Design) | 8 h                |
| Back-end API Design                   | 12 h               |
| Front-end SPA Design                  | 8 h                |
| Deployment                            | 8 h                |

Estimated delivery date of: Monday 13th, EOD (End of Day)

## 2. Architecture



The proposed Architecture, assumes the following elements:

![Context](./doc/Context.png)

- To get bank transactions we resort to a universal Banking System
- To get Credit reports, we resort to a different specialized System.
- To grant a loan, submit payment collections and their respective status we use another System. 

![Container](./doc/Container.png)

To handle the operation, I propose certain services:

1. The Customer Service, it handles the Personal Information, including bank transactions
2. The business Service, it handles the Business Information like credit reports, bank transactions and non-traditional data.
3. The Loan Application Service, it handles the Loan application and all the approval process
4. The Loan Collector Service, with an effective Loan, it handles the automatic payment collection and status.

And additional Authentication (and Authorization) service can keep the requests from the Web Application and the API Gateway in check for correct credentials.

Finally, the Components of the main services could be like this:

![Component](./doc/Component.png) 

## 3. Database

To accomplish this, the database could be like this:

![DB](./doc/DB.png)



## 4. App

### 	a. Front End

From the project directory:

```sh
cd frontend
npm i
```

#### dev (with vue-cli)

```sh
cd frontend
#create local .env
cat  <<'EOF' > .env.development.local
VUE_APP_BACKEND_URL=http://127.0.0.1:8000/api/v1
EOF
npm run serve
```

#### build

```sh
cd frontend
npm run build
```

It builds directly into `backend/static/dist`

### 	b. Back End

From the project directory:

```sh
pip3 install -r backend/requirements.txt
PORT=8000 python3 backend
```

Visit `http://localhost:8000/swagger` to try to POST to `/api/v1/loan-apply`

#### 		Test!

​		From project directory:

> ```sh
> python3 -m unittest
> ```
>

#### dev (with [nodemon](https://nodemon.io/))

> ```sh
> LOGLEVEL=DEBUG PORT=8000 nodemon -e py --exec python3 backend
> ```

with tests

> ```sh
> LOGLEVEL=DEBUG PORT=8000 nodemon -e py --exec 'python3 -m unittest && python3 backend'
> ```



### 	c. git URL: 

https://gitlab.com/arturo-hernandez/lf-exercise

### 	d. test URL: 

https://ahc-lf.herokuapp.com/

## 5. Time Spent

| **Tasks**                             | Worked TIme |
| ------------------------------------- | ----------- |
| Architecture & DB (Research & Design) | 8 h         |
| Back-end API Design                   | 12 h        |
| Front-end SPA Design                  | 10 h        |
| Deployment                            | 8 h         |


The front-end design took more time, because I opted for a Schema-template form with [Vjsf](https://www.npmjs.com/package/@koumoul/vjsf) which didn't work at first.
