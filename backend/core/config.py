#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Application Configuration"""

# pip install dotenv
from os import environ
from dotenv import load_dotenv


def str2bool(anystring):
    """string to bool"""
    return anystring.lower() in ("yes", "true", "t", "1")


load_dotenv()

DEBUG = str2bool(environ.get("DEBUG", "False"))
PORT = int(environ.get("PORT", "80"))
ALLOW_ORIGIN = environ.get("ALLOW_ORIGIN", "*")
API_BASE_PATH = environ.get("API_BASE_PATH", "/api/v1")
