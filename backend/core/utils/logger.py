#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Initialize and handle logging for system"""
import os
from os.path import dirname, abspath
import sys
import logging
from logging import getLogger
from threading import Thread

# change CWD to parent of src/
os.chdir(dirname(dirname(dirname(abspath(__file__)))))
# print (os.getcwd())

# Initial configuration
LEVEL_NAME = os.getenv("LOGLEVEL", "INFO").upper()
if hasattr(logging, LEVEL_NAME):
    LOG_LEVEL = getattr(logging, LEVEL_NAME)
else:
    LOG_LEVEL = logging.INFO  # default

LOG_FILE = os.getenv("LOGFILE", "/dev/stdout")

# if file does not exsits, create it!
if not os.path.isfile(LOG_FILE):
    try:
        open(LOG_FILE, "a").close()
    except OSError:
        print("WRN: can't create file", LOG_FILE)
logging_config = {
    "format": "%(levelname)s %(asctime)s %(name)s - %(message)s",
    "datefmt": "\033[1;37m%Y-%m-%d %H:%M:%S\033[1;0m",
    "level": LOG_LEVEL,
}
if os.path.isfile(LOG_FILE) and os.access(LOG_FILE, os.W_OK):
    logging_config["filename"] = LOG_FILE
else:
    print("WRN: LOG to STDOUT")

logging.addLevelName(logging.DEBUG, "\033[1;36mDBG\033[1;0m")
logging.addLevelName(logging.INFO, "\033[1;34mINF\033[1;0m")
logging.addLevelName(logging.WARNING, "\033[1;33mWRN\033[1;0m")
logging.addLevelName(logging.ERROR, "\033[1;41mERR\033[1;0m")
logging.basicConfig(**logging_config)

LOGGER = getLogger(__name__)


def handle_exception(exc_type, exc_value, exc_traceback):
    """captures exceptions"""
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    LOGGER.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


sys.excepthook = handle_exception
INIT_OLD = Thread.__init__


def init(self, *args, **kwargs):
    """new init with hooks"""
    INIT_OLD(self, *args, **kwargs)
    run_old = self.run

    def run_with_except_hook(*args, **kw):
        """run with hook"""
        try:
            run_old(*args, **kw)
        except (KeyboardInterrupt, SystemExit):
            raise  # final break
        except:  # pylint: disable=bare-except
            sys.excepthook(*sys.exc_info())  # hook trigger

    self.run = run_with_except_hook


Thread.__init__ = init

if __name__ == "__main__":
    LOGGER.debug("hello debug")
    LOGGER.info("hello info")
    LOGGER.warning("hello warn")
    LOGGER.error("hello error")
