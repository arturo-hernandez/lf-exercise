#!/usr/bin/env python
"""
from core.utils import import_directory_modules
"""
import re
from os import listdir
from os.path import join, isdir
from importlib import import_module
from typing import Any, List
from core.utils.logger import getLogger

LOGGER = getLogger(__name__)


def import_directory_modules(
    directory: str, base: str = "", namespace: str = __name__
) -> List[Any]:
    """
    import_directory_modules - Recursively import from a base directory
    """
    module_list = []
    for filename in listdir(directory):
        if re.match(r"^__", filename):
            continue  # ignore __ files
        path = join(directory, filename)
        if isdir(path):
            LOGGER.info("Entering directory /%s", filename)
            inner_modules = import_directory_modules(path, "." + filename, namespace)
            module_list += inner_modules
            continue
        mod_name = filename[:-3]  # strip .py at the end
        LOGGER.debug("Importing: %s", base + "." + mod_name)
        module = import_module(base + "." + mod_name, package=namespace)
        module_list.append(module)
    return module_list
