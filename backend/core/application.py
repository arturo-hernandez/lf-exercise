"""
from core.application import make_web_app
generates an tornado.web.Application with defined routes
including static (swagger) and api routeHandlers
"""
from typing import Any, List, Tuple
from tornado.web import Application, StaticFileHandler
from core.config import DEBUG
from core.utils.logger import getLogger
from api import get_api_handlers

LOGGER = getLogger(__name__)


def swagger_handler() -> Tuple[str, Any]:
    """builds the static swagger FileHandler"""
    return (
        r"/swagger/(.*)",
        StaticFileHandler,
        {"path": "static/swagger", "default_filename": "index.html"},
    )


def frontend_handler() -> Tuple[str, Any]:
    """
    catch-all static frontend
    SHOUL BE THE LAST ROUTE
    """
    return (
        r"/(.*)",
        StaticFileHandler,
        {"path": "static/dist", "default_filename": "index.html"},
    )


def get_handlers() -> List[Tuple[str, Any]]:
    """override api handlers with static handlers"""
    handlers = get_api_handlers()
    handlers.append(swagger_handler())
    handlers.append(frontend_handler())
    return handlers


def make_web_app() -> Application:
    """creates the tornado.web.Application with handlers and extra config"""
    app = Application(get_handlers(), debug=DEBUG)
    return app
