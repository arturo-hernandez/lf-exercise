#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
LF Interview Exercise
# pip3 install tornado
"""

from tornado.ioloop import IOLoop
from core.config import PORT
from core.utils.logger import getLogger
from core.application import make_web_app

LOGGER = getLogger(__name__)

if __name__ == "__main__":
    app = make_web_app()
    app.listen(PORT)
    print("               __  ")
    print("              / _) ")
    print("     _/\\/\\/\\_/ /   ")
    print("   _|         /    ")
    print(" _|  (  | (  |     ")
    print("/__.-'|_|--|_|  AHC")
    print("")
    print("Starting  http://127.0.0.1:%s" % PORT)
    LOGGER.info("App Listening on http://127.0.0.1:%s", PORT)
    try:
        IOLoop.current().start()
    except KeyboardInterrupt:
        IOLoop.current().stop()
