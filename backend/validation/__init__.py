#!/usr/bin/env python
"""
validation  schema loading and validate
"""
import json
from os.path import realpath, join
from typing import Dict
from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError
from core.utils.logger import getLogger

LOGGER = getLogger(__name__)
SCHEMA_LOCATION = "validation/schema/"


def load_schema(filename: str):
    """
    Loads the schema and parse JSON
    """
    with open(realpath(join(SCHEMA_LOCATION, filename))) as file:
        schema = json.load(file)
    return schema


def is_invalid(data: Dict, schema_file: str):
    """
    return false if data is valid,
    else, return the field which didn't match the schema criteria
    """
    schema = load_schema(schema_file)
    try:
        validate(instance=data, schema=schema)
    except SchemaError as error:
        LOGGER.error("Invalid schema file %s", schema_file)
        LOGGER.debug(error)
    except ValidationError as error:
        LOGGER.warning(error.message)
        LOGGER.debug(error)
        field = error.schema
        return field
    return False
