#!/usr/bin/env python
"""
Loan Application controller
"""
from typing import Callable, Dict
from api.apihandler import api_response
from core.utils.logger import getLogger
from .controller import ApiController

LOGGER = getLogger(__name__)


class LoanAmountDecider(object):
    """basic decider"""

    def __init__(self, decider: Callable, title: str, message: str) -> None:
        """init"""
        self.decider = decider
        self.title = title
        self.message = message

    def decide(self, request_amount: int) -> bool:
        """calls decider"""
        return self.decider(request_amount)

    def format_response(self) -> Dict:
        """returns api response"""
        return api_response(message_type=self.title, message=self.message)


class LoanApplicationDecision(object):
    """Type of decision"""

    DecisionTable = [
        LoanAmountDecider(
            lambda request_amount: request_amount > 50000,
            "Declined",
            "We're sorry! your loan has been declined.",
        ),
        LoanAmountDecider(
            lambda request_amount: request_amount == 50000,
            "Undecided",
            "Sorry! your loan can't be decided.",
        ),
        LoanAmountDecider(
            lambda request_amount: request_amount < 50000,
            "Approved",
            "Congratulations! your loan has been approved.",
        ),
    ]

    @classmethod
    def decide(cls, request_amount):
        """evaluates each case from the decision table and returns the first  to match"""
        taken = None
        for decider in cls.DecisionTable:
            if decider.decide(request_amount):
                taken = decider
                break
        return taken


class LoanApplicationController(ApiController):
    """Loan Application"""

    def __init__(self, data: Dict) -> None:
        self.data = data

    def evaluate(self):
        """use of enum"""
        request_amount = self.data["requestAmount"]
        decider = LoanApplicationDecision.decide(request_amount)
        return decider.format_response()
