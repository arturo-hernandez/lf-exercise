#!/usr/bin/env python
"""
Schema files Handler
"""
from typing import Any, Tuple
from tornado.web import StaticFileHandler
from .apihandler import ApiHandler

# pylint: disable=W0223
class SchemaFileHandler(StaticFileHandler, ApiHandler):
    """
    Schema static handler, serves from ./validation/schema
    """

    path = r"/schema/(.*)"

    @classmethod
    def build_handler(cls) -> Tuple[Any, Any, Any]:
        """custom handler builder for extra arguments"""
        return (
            cls.get_path(),
            cls,
            {"path": "validation/schema", "default_filename": "index.html"},
        )
