"""
from .api import get_api_handlers
Tornado API Handler Factory
"""
from typing import Any, List, Tuple
from os.path import dirname
from inspect import getmembers, isclass
from tornado.web import RequestHandler
from core.utils.logger import getLogger
from core.utils import import_directory_modules

LOGGER = getLogger(__name__)


def get_api_handlers() -> List[Tuple[str, Any]]:
    """
    get_api_handlers() Tornado API Handler Factory
    It loads the current directory for valid RequestHandler definitions
    Tuple(path,handler) and returns a List of them
    """
    from_path = dirname(__file__)
    module_list = import_directory_modules(from_path, namespace=__name__)
    handlers = []
    for module in module_list:
        for name, controller in getmembers(module, isclass):
            if not issubclass(controller, RequestHandler):
                LOGGER.debug("Ignoring %s.%s", module.__name__, name)
                continue
            if not hasattr(controller, "path") or controller.path is None:
                continue  # ignores base class
            path = controller.get_path()
            LOGGER.debug("Loading handler %s for %s", name, path)
            handlers.append(controller.build_handler())
    return handlers
