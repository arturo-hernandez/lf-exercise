"""
Base ApiHandler with CORS, path, response and route builder
"""

import re
from typing import Any, Tuple, Dict
from json.decoder import JSONDecodeError
from tornado.web import RequestHandler, Finish
from tornado.escape import json_decode
import core.config as config
from core.utils.logger import getLogger
from validation import is_invalid

LOGGER = getLogger(__name__)


def api_response(code=200, message_type="OK", message="Operation successful"):
    """
    Default Response Builder
    """
    return {"code": code, "type": message_type, "message": message}


# pylint: disable=W0223
class ApiHandler(RequestHandler):
    """
    Base ApiHandler for CORS and path
    """

    _base_path: str = config.API_BASE_PATH
    path: Any = None  # template for path definition
    validation_schema: str = None

    @classmethod
    def build_handler(cls) -> Tuple[Any, Any]:
        """
        builds a complete Tuple[path, handler] for this class
        """
        LOGGER.debug((cls.get_path(), cls))
        return (cls.get_path(), cls)

    @classmethod
    def get_path(cls) -> Any:
        """
        Generate path string/regex
        """
        if isinstance(cls.path, str):
            return "".join((cls._base_path, cls.path))
        else:
            return re.compile(cls._base_path + cls.path)

    def api_response(
        self,
        code: int = 200,
        message_type: str = "OK",
        message: str = "Operation successful",
    ):
        """
        finish with an api response
        """
        self.set_status(code)
        raise Finish(api_response(code, message_type, message))

    def decode_body(self) -> Dict:
        """check and decode body"""
        if not self.request.body:
            self.api_response(400, "missing data", "missing data")
            return
        try:
            return json_decode(self.request.body)
        except JSONDecodeError:
            self.api_response(400, "Bad Request", "Malformed JSON")

    def validate(self, data) -> bool:
        """finish if invalid an return 400"""
        if not self.validation_schema:
            return True  # if no schema no need to check
        field = is_invalid(data, self.validation_schema)
        if field:
            LOGGER.warning("invalid data %s", field["$id"])
            self.api_response(400, "Bad Request", field["description"])
            return False
        return True

    def set_default_headers(self) -> None:
        """
        build Headers for CORS.
        Allow-Origin : default * but configurable via environment ALLOW_ORIGIN
        Allow-Methods: built from current RequestHandler
        Allow-Headers: x-requested-with,access-control-allow-origin,
                       authorization,content-type
        """
        # build list:
        methods = ["head", "get", "post", "delete", "patch", "put", "options"]
        filtered = [
            mn.upper()
            for mn in methods
            if getattr(self, mn, None) != self._unimplemented_method
        ]
        self.set_header("Access-Control-Allow-Origin", config.ALLOW_ORIGIN)
        self.set_header("Access-Control-Allow-Methods", ", ".join(filtered))
        self.set_header(
            "Access-Control-Allow-Headers",
            "x-requested-with,access-control-allow-origin,authorization,content-type",
        )
        remote_ip = (
            self.request.headers.get("X-Real-IP")
            or self.request.headers.get("X-Forwarded-For")
            or self.request.remote_ip
        )
        LOGGER.info("%s from %s", self.request.method, remote_ip)

    def options(self, *args: Any, **kwargs: Any) -> None:
        """/OPTIONS for CORS. return only headers"""
        self.set_status(204)
        LOGGER.debug("who wants cors? %s, %s", args, kwargs)
        # no body: could return model?
        self.finish()

    def write_error(self, status_code: int, **kwargs: Any) -> None:
        """Override to implement custom error pages."""
        self.finish(api_response(status_code, self._reason, self._reason))
