#!/usr/bin/env python
"""
Loan Application Route Controller
"""

from core.utils.logger import getLogger
from controller.loan_application import LoanApplicationController as Controller
from .apihandler import ApiHandler

LOGGER = getLogger(__name__)

# pylint: disable=W0223
class LoanApplicationHandler(ApiHandler):
    """Loan Application Handler"""

    path = "/loan-application"
    validation_schema = "loan-application.json"

    async def post(self) -> None:
        """POST ({json}) should validate and eval the application"""
        data = self.decode_body()
        LOGGER.debug(data)
        self.validate(data)
        controller = Controller(data)
        response = controller.evaluate()
        self.finish(response)
