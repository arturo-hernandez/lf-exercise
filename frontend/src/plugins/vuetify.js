import Vue from 'vue';
//import Vuetify from 'vuetify/lib/framework';

//Vue.use(Vuetify);

import Vuetify, {
  VRow,
  VCol,
  VBtn,
  VTextField,
  VTooltip,
  VCheckbox,
  VCombobox,
  VSelect,
  VIcon,
  VInput, //
  VMenu, //
  VListItemContent,
  VListItemTitle
} from 'vuetify/lib'

import { Ripple, Intersect, Touch, Resize } from 'vuetify/lib/directives'
Vue.use(Vuetify, {
  components: {
    VRow, VTooltip, VCol, VBtn,
    VTextField, VCheckbox,
    VCombobox, VSelect, VIcon,
    VInput, VMenu, VListItemContent,
    VListItemTitle
  }, directives: { Ripple, Intersect, Touch, Resize }
});

export default new Vuetify({
});
